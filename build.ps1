Push-Location "$PSScriptRoot"

Import-Module .\build\build-functions.psm1 -Force

$msbuild = Get-MsBuildPath
#$nuget = "NuGet.exe"
$BuildNumber = @{ $true = $env:CI_PIPELINE_ID; $false=0 }[$env:CI_PIPELINE_ID -ne $NULL];

try {
    $CommitHash = $(git rev-parse --short HEAD)
    
    Write-Host "BuildNumber: $BuildNumber"
    Write-Host "CommitHash: $CommitHash"
    
    If (Test-Path ".\artifacts") { 
        Remove-Item -Recurse -Force ".\artifacts\*"
    }
    Else { 
        New-Item -ItemType Directory .\artifacts\ 
    }       
    
    #Exec { & $nuget restore .\ } "Error while restoring nuget packages."

    Exec { & $msbuild "src\Anton.EmployeePlanning\Anton.EmployeePlanning.csproj" /t:Build /p:Configuration=Release`;BuildNumber=$BuildNumber`;CommitHash=$CommitHash } "Error while building project."    

    Exec { & $msbuild "src\Setup\Setup.wixproj" /t:Build /p:Configuration=Release /p:Configuration=Release`;BuildNumber=$BuildNumber`;CommitHash=$CommitHash } "Error while building setup."
    Copy-Item -Path .\src\Setup\bin\Release\*.msi -Destination .\artifacts\
}
Finally {
    Pop-Location
}
