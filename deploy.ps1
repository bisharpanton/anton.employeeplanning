Push-Location "$PSScriptRoot"
$artifacts = ".\artifacts\*"
$destinationRoot = "\\rds-nas2\ftp\Anton\"
$destinationFolder = "Personeelsplanning"

$destination = "$destinationRoot\$destinationFolder"

try {
    if (-Not (Test-Path $destinationRoot)) {
        throw "Folder does not exist: $destinationRoot"
    }

    if (-Not (Test-Path $destination)) {
        New-Item -ItemType Directory $destination
    }
    
    Copy-Item -Path $artifacts -Destination $destination
}
Finally {
    Pop-Location
}
