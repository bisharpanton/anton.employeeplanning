﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class Contact
    {
        [ForeignKey("FK_PERSON")]
        [Include]
        public Person Person { get; set; }
    }
}
