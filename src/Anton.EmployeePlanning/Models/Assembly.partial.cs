﻿using System.Collections.Generic;
using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class Assembly
    {
        [HasMany("FK_ASSEMBLY")]
        [Include]
        public IList<AssemblyDetailWorkActivity> AssemblyDetailWorkActivities { get; set; }
    }
}
