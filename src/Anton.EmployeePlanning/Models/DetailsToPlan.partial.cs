﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class DetailsToPlan
    {
        [ForeignKey("FK_SALESORDERDETAILASSEMBLY")]
        [Include]
        public SalesOrderDetailAssembly SalesOrderDetailAssembly { get; set; }

        [ForeignKey("FK_JOBORDERDETAILWORKACTIVITY")]
        [Include]
        public JobOrderDetailWorkActivity JobOrderDetailWorkActivity { get; set; }
    }
}
