﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class Order
    {
        [ForeignKey("FK_RELATION")]
        [Include]
        public Relation Relation { get; set; }
    }
}
