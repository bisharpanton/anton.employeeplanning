﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class JobOrderDetailWorkActivity
    {
        [ForeignKey("FK_WORKACTIVITY")]
        [Include]
        public WorkActivity WorkActivity { get; set; }

        [ForeignKey("FK_JOBORDER")]
        [Include]
        public JobOrder JobOrder { get; set; }
    }
}
