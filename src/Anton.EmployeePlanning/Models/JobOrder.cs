//  <auto-generated />
namespace Anton.EmployeePlanning.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_JOBORDER
    ///  </summary>
    ///  	ADDITIONALWORK, ADDRESSLOCATION, BARCODE1, BARCODE2, BATCHDEPENDENT, BESTELDM3PERUUR, BETONSOORT, BUDGET, BUDGETQUANTITYHOURS, BUDGETTOTALITEMCOSTPRICE
    ///  	BUDGETTOTALMISCCOSTPRICE, BUDGETTOTALOUTSOURCECOSTPRICE, BUDGETTOTALWORKCOSTPRICE, CORRECTIONQUANTITYHOURS, CORRECTIONTOTALITEMCOSTPRICE, CORRECTIONTOTALMISCCOSTPRICE, CORRECTIONTOTALOUTSOURCECOSTPRICE, CORRECTIONTOTALWORKCOSTPRICE, CREATOR, DATECHANGED
    ///  	DATECREATED, DATEFINANCIALFINISHED, DATEPRODUCTIONFINISHED, DELIVERYDATE, DESCRIPTION, DRAWINGNUMBER, ENDTIME, EXTERNALKEY, FINANCIALDONE, FIXEDBUDGET
    ///  	FIXEDPLANNING, FK_ALTERNATIVESERVICEADDRESS, FK_ASSEMBLY, FK_ASSEMBLYDETAILSUBASSEMBLY, FK_BETONCENTRALE, FK_CALLCODE, FK_LOT, FK_METERREADER, FK_OBJECTLOCATION, FK_ORDER
    ///  	FK_ORDERBASE, FK_POMPLEVERANCIER, FK_PRODUCTTYPE, FK_R_JOBORDER, FK_SERVICECONTRACT, FK_SERVICEINTERVAL, FK_SERVICEMECHANIC, FK_SERVICEOBJECT, FK_SERVICETYPE, FK_SUBORDER
    ///  	FK_WAREHOUSE, FK_WORKFLOWSTATE, HANDMATIGAANGEPAST, INTERNALMEMO, ISPRODUCTIONDONE, ISRELEASEDFORPRODUCTION, ISSERVICEPLANNED, JOBORDERNUMBER, KEYWORDS, LEIDINGKAR
    ///  	LEVEL, MAXIMALPRODUCTIONFINISHDATE, MEMO, METERREADING, MINIMALPRODUCTIONSTARTDATE, OORSPRONKELIJKBUDGET, OPHAALDATUM, ORIGINALSUBASSEMBLYQUANTITY, PK_R_JOBORDER, PLAINTEXT_INTERNALMEMO
    ///  	PLAINTEXT_MEMO, PLAINTEXT_POSSIBLESOLUTION, PLAINTEXT_REALIZEDSOLUTION, PLANDIRECTION, PLANNEDDURATION, PLANNEDPRODUCTIONENDDATE, PLANNEDPRODUCTIONSTARTDATE, PLANSETTING, PLANSTATE, POSITION
    ///  	POSSIBLESOLUTION, PREPRODUCTION, PRIJSAFSPRAAK, PRIORITY, PRODUCTIONDONE, PRODUCTIONRESULT, QUANTITYDEFECT, QUANTITYPLANNED, REALIZEDSOLUTION, RECORDLINK
    ///  	REMOTESERVICESTATE, REQUESTEDSTARTDATE, SALESSTATE, SENDDATE, SMEERBED, SPECIFICATIONFILE, SPECIFICATIONNUMBER, STARTDATE, STARTTIJD, TIMEANDMATERIAL
    ///  	TOTALINVOICEDINADVANCEAMOUNT, USERCHANGED, VELLINGKANT, VERTREKTIJD, VOORTGANG
    ///  
    ///  	Filter used: FK_TABLEINFO = '7de0ac3e-9dfa-47e0-b113-63d3cac55f90' AND (ISSECRET = 0)
    ///  	Included columns: JOBORDERNUMBER, DESCRIPTION, FK_ORDER
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_JOBORDER")]
    public partial class JobOrder
    {
        
        private string _DESCRIPTION;
        
        private int _FK_ORDER;
        
        private int _JOBORDERNUMBER;
        
        private int _id;
        
        /// <summary>
        /// 	Description
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 80
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Order
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_JOBORDER
        /// </remarks>
        [Column("FK_ORDER")]
        public virtual int FK_ORDER
        {
            get
            {
                return this._FK_ORDER;
            }
            set
            {
                this._FK_ORDER = value;
            }
        }
        
        /// <summary>
        /// 	Job order number
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("JOBORDERNUMBER")]
        public virtual int JOBORDERNUMBER
        {
            get
            {
                return this._JOBORDERNUMBER;
            }
            set
            {
                this._JOBORDERNUMBER = value;
            }
        }
        
        /// <summary>
        /// 	Job order id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_JOBORDER")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}
