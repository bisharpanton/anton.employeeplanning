﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class SalesOrderDetailAssembly
    {
        [ForeignKey("FK_ORDER")]
        [Include]
        public Order Order { get; set; }

        [ForeignKey("FK_ASSEMBLY")]
        [Include]
        public Assembly Assembly { get; set; }
    }
}
