﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class AssemblyDetailWorkActivity
    {
        [ForeignKey("FK_WORKACTIVITY")]
        [Include]
        public WorkActivity WorkActivity { get; set; }
    }
}
