﻿using System.Collections.Generic;
using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class PlannedDaysPerJobOrder
    {
        [ForeignKey("FK_JOBORDERDETAILWORKACTIVITY")]
        [Include]
        public JobOrderDetailWorkActivity JobOrderDetailWorkActivity { get; set; }

        [HasMany("FK_PLANNEDDAYSPERJOBORDERDETAIL")]
        [Include]
        public List<PlannedEmployeesPerDate> Employees { get; set; }
    }
}
