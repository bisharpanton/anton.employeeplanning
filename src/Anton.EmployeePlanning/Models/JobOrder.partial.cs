﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class JobOrder
    {
        [ForeignKey("FK_ORDER")]
        [Include]
        public Order Order { get; set; }
    }
}
