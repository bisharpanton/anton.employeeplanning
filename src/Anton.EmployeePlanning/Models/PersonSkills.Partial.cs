﻿using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Models
{
    public partial class PersonSkills
    {
        [ForeignKey("FK_EMPLOYEE")]
        [Include]
        public Employee Employee { get; set; }

        [ForeignKey("FK_CONTACT")]
        [Include]
        public Contact Contact { get; set; }
    }
}
