﻿using System.Collections.Generic;
using System.Windows.Forms;
using Anton.EmployeePlanning.Models;

namespace Anton.EmployeePlanning.Helpers
{
    public static class PlanningExtensions
    {
        /*
        public static TreeNode ToTreeNode(this JobOrderDetailWorkActivity jodwa)
        {
            var node = new TreeNode(string.Format("{0}\t{1}", jodwa.WorkActivity.CODE, jodwa.DESCRIPTION));
            node.Tag = jodwa;
            return node;
        }

        public static ListViewItem ToListViewItem(this JobOrderDetailWorkActivity jodwa)
        {
            var item = new ListViewItem(new[] { jodwa.WorkActivity.CODE, jodwa.DESCRIPTION });
            item.Tag = jodwa;
            return item;
        }
        */

        public static TreeNode ToTreeNode(this AssemblyDetailWorkActivity adwa)
        {
            var node = new TreeNode($"{adwa.WorkActivity.CODE}\t{adwa.DESCRIPTION}")
            {
                Tag = adwa
            };
            return node;
        }

        public static TreeNode ToTreeNode(this DetailsToPlan dtp)
        {
            var node = new TreeNode();
            if (dtp.FK_JOBORDERDETAILWORKACTIVITY.HasValue)
            {
                var wa = dtp.JobOrderDetailWorkActivity.WorkActivity;
                node.Text = $@"{wa.CODE} {wa.DESCRIPTION}";
            }
            else if (dtp.FK_SALESORDERDETAILASSEMBLY.HasValue)
            {
                // Hier kan logica voor Verkoopregels ingebouwd worden
            }
            node.Tag = dtp;
            return node;
        }

        public static ListViewItem ToListViewItem(this PlannedDaysPerJobOrder pdpjo)
        {
            var wa = pdpjo.JobOrderDetailWorkActivity.WorkActivity;
            var orderNumber = pdpjo.JobOrderDetailWorkActivity.JobOrder.Order.ORDERNUMBER;
            var jobOrderNumber = pdpjo.JobOrderDetailWorkActivity.JobOrder.JOBORDERNUMBER;
            var numberCombined = $"{orderNumber}.{jobOrderNumber}";
            var relationName = pdpjo.JobOrderDetailWorkActivity.JobOrder.Order.Relation.NAME;
            var orderDescription = pdpjo.JobOrderDetailWorkActivity.JobOrder.Order.DESCRIPTION;

            var item = new ListViewItem(new[] { wa.CODE, wa.DESCRIPTION, numberCombined, relationName, orderDescription })
            {
                Tag = pdpjo
            };
            //item.ToolTipText = $"{wa.CODE} | {wa.DESCRIPTION} | {numberCombined} | {relationName} | {orderDescription}";
            return item;
        }

        public static TreeNode ToTreeNode(this PlannedEmployeesPerDate pepd)
        {
            var node = new TreeNode();

            if (pepd.Contact != null)
            {
                node.Text = "" + pepd.Contact.Person.FIRSTNAME;
            }
            else
            {
                node.Text = "" + pepd.Employee.Person.FIRSTNAME;
            }
            node.Tag = pepd;

            return node;
        }

        public static ListViewItem ToListViewItem(this Employee e)
        {
            return new ListViewItem(new[] { e.CODE, e.Person?.FIRSTNAME, e.Person?.NAMEPREFIX, e.Person?.LASTNAME })
            {
                Tag = e
            };
        }

        public static ListViewItem ToListViewItem(this Contact c)
        {
            return new ListViewItem(new[] { c?.Person?.FIRSTNAME, c?.Person?.NAMEPREFIX, c?.Person?.LASTNAME })
            {
                Tag = c
            };
        }

        public static ListViewItem[] ToListViewItemArray(this List<Employee> employees)
        {
            var array = new ListViewItem[employees.Count];
            var index = 0;
            foreach (var employee in employees)
            {
                array[index] = employee.ToListViewItem();
            }
            return array;
        }

        public static ListViewItem[] ToListViewItemArray(this List<Contact> contacts)
        {
            var array = new ListViewItem[contacts.Count];
            var index = 0;
            foreach (var contact in contacts)
            {
                array[index] = contact.ToListViewItem();
            }
            return array;
        }
    }
}
