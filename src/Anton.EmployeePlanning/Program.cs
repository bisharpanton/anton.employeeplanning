﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anton.EmployeePlanning.Forms;
using Anton.EmployeePlanning.Properties;
using NLog;

namespace Anton.EmployeePlanning
{
    public class Program
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                {
                    var exception = (Exception)args.ExceptionObject;
                    HandleException(exception);
                };

            Application.ThreadException += (sender, args) =>
                {
                    HandleException(args.Exception);
                };

            TaskScheduler.UnobservedTaskException += (sender, args) =>
                {
                    HandleException(args.Exception);
                };

            if (Settings.Default.NeedsSettingUpgrade)
            {
                Settings.Default.Upgrade();
                Settings.Default.NeedsSettingUpgrade = false;
                Settings.Default.Save();
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WeekPlanning());
        }

        static void HandleException(Exception exception)
        {
            try
            {
                MessageBox.Show(exception.ToString());

                _log.Fatal(exception, exception.Message);
                LogManager.Flush();

                Application.Exit();
            }
            catch (Exception e)
            {
                // Ignore
#if DEBUG
                        MessageBox.Show(e.ToString());
#endif
            }

        }
    }
}
