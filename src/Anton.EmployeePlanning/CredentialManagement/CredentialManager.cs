﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace Anton.EmployeePlanning.CredentialManagement
{
    public static class CredentialManager
    {
        /// <summary>
        /// Extract the stored credential from WIndows Credential store
        /// </summary>
        /// <param name="Target">Name of the application/Url where the credential is used for</param>
        /// <returns>null if target not found, else stored credentials</returns>
        public static NetworkCredential GetCredentials(string Target)
        {
            IntPtr nCredPtr;
            var username = String.Empty;
            var passwd = String.Empty;
            var domain = String.Empty;

            // Make the API call using the P/Invoke signature
            bool succeeded = NativeCode.CredRead(Target, NativeCode.CredentialType.Generic, 0, out nCredPtr);
            // TODO: Turned off for now, maybe check error codes
            int lastError = Marshal.GetLastWin32Error();
            if (!succeeded)
            {
            //    throw new Win32Exception(lastError);
                return null;
            }

            // If the API was successful then...
            if (succeeded)
            {
                using (CriticalCredentialHandle critCred = new CriticalCredentialHandle(nCredPtr))
                {
                    Credential cred = critCred.GetCredential();
                    if (cred == null)
                    {
                        return null;
                    }
                    passwd = cred.CredentialBlob;
                    var user = cred.UserName;
                    StringBuilder userBuilder = new StringBuilder();
                    StringBuilder domainBuilder = new StringBuilder();
                    var ret1 = NativeCode.CredUIParseUserName(user, userBuilder, int.MaxValue, domainBuilder, int.MaxValue);
                    lastError = Marshal.GetLastWin32Error();

                    //assuming invalid account name to be not meeting condition for CredUIParseUserName 
                    //"The name must be in UPN or down-level format, or a certificate"
                    if (ret1 == NativeCode.CredentialUIReturnCodes.InvalidAccountName)
                        userBuilder.Append(user);
                    else if ((uint)ret1 > 0)
                        //throw new Win32Exception(lastError, "CredUIParseUserName threw an error");
                        return null;

                    username = userBuilder.ToString();
                    domain = domainBuilder.ToString();
                    return new NetworkCredential(username, passwd, domain);
                }
            }
            return null;
        }

        /// <summary>
        /// Saves teh given Network Credential into Windows Credential store
        /// </summary>
        /// <param name="Target">Name of the application/Url where the credential is used for</param>
        /// <param name="credential">Credential to store</param>
        /// <returns>True:Success, False:Failure</returns>
        public static bool SaveCredentials(string Target, NetworkCredential credential)
        {
            // Go ahead with what we have are stuff it into the CredMan structures.
            Credential cred = new Credential(credential);
            cred.TargetName = Target;
            cred.Persist = NativeCode.Persistance.Entrprise;
            NativeCode.NativeCredential ncred = cred.GetNativeCredential();
            // Write the info into the CredMan storage.
            bool written = NativeCode.CredWrite(ref ncred, 0);
            int lastError = Marshal.GetLastWin32Error();
            if (written)
            {
                return true;
            }
            else
            {
                string message = string.Format("CredWrite failed with the error code {0}.", lastError);
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Remove stored credentials from windows credential store
        /// </summary>
        /// <param name="Target">Name of the application/Url where the credential is used for</param>
        /// <returns>True: Success, False: Failure</returns>
        public static bool RemoveCredentials(string Target)
        {
            // Make the API call using the P/Invoke signature
            var ret = NativeCode.CredDelete(Target, NativeCode.CredentialType.Generic, 0);
            int lastError = Marshal.GetLastWin32Error();
            if (!ret)
                throw new Win32Exception(lastError, "CredDelete threw an error");
            return ret;
        }
    }
}
