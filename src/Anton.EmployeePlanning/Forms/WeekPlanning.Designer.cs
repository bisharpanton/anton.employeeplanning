﻿using Anton.EmployeePlanning.CalendarEx;

namespace Anton.EmployeePlanning.Forms
{
    partial class WeekPlanning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeekPlanning));
            this.tvJobOrders = new System.Windows.Forms.TreeView();
            this.lvMonday = new System.Windows.Forms.ListView();
            this.jobOrderDetailWorkActivityCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jobOrderDetailWorkActivityDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jobOrdeDetailWorkActivityOrderInfo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvTuesday = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvWednesday = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvThursday = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFriday = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvSaturday = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblMonday = new System.Windows.Forms.Label();
            this.lblTuesday = new System.Windows.Forms.Label();
            this.lblWednesday = new System.Windows.Forms.Label();
            this.lblThursday = new System.Windows.Forms.Label();
            this.lblFriday = new System.Windows.Forms.Label();
            this.lblSaturday = new System.Windows.Forms.Label();
            this.lblToPlan = new System.Windows.Forms.Label();
            this.rootPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gbFilter = new System.Windows.Forms.GroupBox();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tvSalesOrders = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblWeek = new System.Windows.Forms.Label();
            this.calendar = new Anton.EmployeePlanning.CalendarEx.MonthCalendarEx();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lblPlannedPeople = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rootPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.gbFilter.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvJobOrders
            // 
            this.tvJobOrders.AllowDrop = true;
            this.tvJobOrders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvJobOrders.Location = new System.Drawing.Point(0, 452);
            this.tvJobOrders.Name = "tvJobOrders";
            this.tvJobOrders.Size = new System.Drawing.Size(584, 265);
            this.tvJobOrders.TabIndex = 5;
            // 
            // lvMonday
            // 
            this.lvMonday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvMonday.AllowDrop = true;
            this.lvMonday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.jobOrderDetailWorkActivityCode,
            this.jobOrderDetailWorkActivityDescription,
            this.jobOrdeDetailWorkActivityOrderInfo,
            this.columnHeader17,
            this.columnHeader16});
            this.lvMonday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMonday.FullRowSelect = true;
            this.lvMonday.Location = new System.Drawing.Point(3, 16);
            this.lvMonday.Name = "lvMonday";
            this.lvMonday.Size = new System.Drawing.Size(625, 96);
            this.lvMonday.TabIndex = 3;
            this.lvMonday.Tag = "1";
            this.lvMonday.UseCompatibleStateImageBehavior = false;
            this.lvMonday.View = System.Windows.Forms.View.Details;
            this.lvMonday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvMonday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // jobOrderDetailWorkActivityCode
            // 
            this.jobOrderDetailWorkActivityCode.Text = "Code";
            this.jobOrderDetailWorkActivityCode.Width = 70;
            // 
            // jobOrderDetailWorkActivityDescription
            // 
            this.jobOrderDetailWorkActivityDescription.Text = "Description";
            this.jobOrderDetailWorkActivityDescription.Width = 130;
            // 
            // jobOrdeDetailWorkActivityOrderInfo
            // 
            this.jobOrdeDetailWorkActivityOrderInfo.Text = "Bon";
            this.jobOrdeDetailWorkActivityOrderInfo.Width = 75;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Relatie";
            this.columnHeader17.Width = 130;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Order omschrijving";
            this.columnHeader16.Width = 200;
            // 
            // lvTuesday
            // 
            this.lvTuesday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvTuesday.AllowDrop = true;
            this.lvTuesday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader11,
            this.columnHeader19,
            this.columnHeader18});
            this.lvTuesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvTuesday.FullRowSelect = true;
            this.lvTuesday.Location = new System.Drawing.Point(3, 131);
            this.lvTuesday.Name = "lvTuesday";
            this.lvTuesday.Size = new System.Drawing.Size(625, 96);
            this.lvTuesday.TabIndex = 6;
            this.lvTuesday.Tag = "2";
            this.lvTuesday.UseCompatibleStateImageBehavior = false;
            this.lvTuesday.View = System.Windows.Forms.View.Details;
            this.lvTuesday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvTuesday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Code";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Bon";
            this.columnHeader11.Width = 75;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Relatie";
            this.columnHeader19.Width = 130;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Order omschrijving";
            this.columnHeader18.Width = 200;
            // 
            // lvWednesday
            // 
            this.lvWednesday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvWednesday.AllowDrop = true;
            this.lvWednesday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader12,
            this.columnHeader21,
            this.columnHeader20});
            this.lvWednesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvWednesday.FullRowSelect = true;
            this.lvWednesday.Location = new System.Drawing.Point(3, 246);
            this.lvWednesday.Name = "lvWednesday";
            this.lvWednesday.Size = new System.Drawing.Size(625, 96);
            this.lvWednesday.TabIndex = 7;
            this.lvWednesday.Tag = "3";
            this.lvWednesday.UseCompatibleStateImageBehavior = false;
            this.lvWednesday.View = System.Windows.Forms.View.Details;
            this.lvWednesday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvWednesday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Code";
            this.columnHeader3.Width = 70;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Description";
            this.columnHeader4.Width = 130;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Bon";
            this.columnHeader12.Width = 75;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Relatie";
            this.columnHeader21.Width = 130;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Order omschrijving";
            this.columnHeader20.Width = 200;
            // 
            // lvThursday
            // 
            this.lvThursday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvThursday.AllowDrop = true;
            this.lvThursday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader13,
            this.columnHeader23,
            this.columnHeader22});
            this.lvThursday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvThursday.FullRowSelect = true;
            this.lvThursday.Location = new System.Drawing.Point(3, 361);
            this.lvThursday.Name = "lvThursday";
            this.lvThursday.Size = new System.Drawing.Size(625, 96);
            this.lvThursday.TabIndex = 8;
            this.lvThursday.Tag = "4";
            this.lvThursday.UseCompatibleStateImageBehavior = false;
            this.lvThursday.View = System.Windows.Forms.View.Details;
            this.lvThursday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvThursday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Code";
            this.columnHeader5.Width = 70;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Description";
            this.columnHeader6.Width = 130;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Bon";
            this.columnHeader13.Width = 75;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Relatie";
            this.columnHeader23.Width = 130;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Order omschrijving";
            this.columnHeader22.Width = 200;
            // 
            // lvFriday
            // 
            this.lvFriday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvFriday.AllowDrop = true;
            this.lvFriday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader14,
            this.columnHeader25,
            this.columnHeader24});
            this.lvFriday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvFriday.FullRowSelect = true;
            this.lvFriday.Location = new System.Drawing.Point(3, 476);
            this.lvFriday.Name = "lvFriday";
            this.lvFriday.Size = new System.Drawing.Size(625, 96);
            this.lvFriday.TabIndex = 9;
            this.lvFriday.Tag = "5";
            this.lvFriday.UseCompatibleStateImageBehavior = false;
            this.lvFriday.View = System.Windows.Forms.View.Details;
            this.lvFriday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvFriday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Code";
            this.columnHeader7.Width = 70;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Description";
            this.columnHeader8.Width = 130;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Bon";
            this.columnHeader14.Width = 75;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Relatie";
            this.columnHeader25.Width = 130;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Order omschrijving";
            this.columnHeader24.Width = 200;
            // 
            // lvSaturday
            // 
            this.lvSaturday.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvSaturday.AllowDrop = true;
            this.lvSaturday.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader15,
            this.columnHeader27,
            this.columnHeader26});
            this.lvSaturday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvSaturday.FullRowSelect = true;
            this.lvSaturday.Location = new System.Drawing.Point(3, 591);
            this.lvSaturday.Name = "lvSaturday";
            this.lvSaturday.Size = new System.Drawing.Size(625, 101);
            this.lvSaturday.TabIndex = 10;
            this.lvSaturday.Tag = "6";
            this.lvSaturday.UseCompatibleStateImageBehavior = false;
            this.lvSaturday.View = System.Windows.Forms.View.Details;
            this.lvSaturday.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListView_ItemDrag);
            this.lvSaturday.DragOver += new System.Windows.Forms.DragEventHandler(this.ListView_DragOver);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Code";
            this.columnHeader9.Width = 70;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Description";
            this.columnHeader10.Width = 130;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Bon";
            this.columnHeader15.Width = 75;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Relatie";
            this.columnHeader27.Width = 130;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Order omschrijving";
            this.columnHeader26.Width = 200;
            // 
            // lblMonday
            // 
            this.lblMonday.AutoSize = true;
            this.lblMonday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMonday.Location = new System.Drawing.Point(3, 0);
            this.lblMonday.Name = "lblMonday";
            this.lblMonday.Size = new System.Drawing.Size(625, 13);
            this.lblMonday.TabIndex = 11;
            this.lblMonday.Text = "Maandag";
            // 
            // lblTuesday
            // 
            this.lblTuesday.AutoSize = true;
            this.lblTuesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTuesday.Location = new System.Drawing.Point(3, 115);
            this.lblTuesday.Name = "lblTuesday";
            this.lblTuesday.Size = new System.Drawing.Size(625, 13);
            this.lblTuesday.TabIndex = 12;
            this.lblTuesday.Text = "Dinsdag";
            // 
            // lblWednesday
            // 
            this.lblWednesday.AutoSize = true;
            this.lblWednesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWednesday.Location = new System.Drawing.Point(3, 230);
            this.lblWednesday.Name = "lblWednesday";
            this.lblWednesday.Size = new System.Drawing.Size(625, 13);
            this.lblWednesday.TabIndex = 13;
            this.lblWednesday.Text = "Woensdag";
            // 
            // lblThursday
            // 
            this.lblThursday.AutoSize = true;
            this.lblThursday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThursday.Location = new System.Drawing.Point(3, 345);
            this.lblThursday.Name = "lblThursday";
            this.lblThursday.Size = new System.Drawing.Size(625, 13);
            this.lblThursday.TabIndex = 14;
            this.lblThursday.Text = "Donderdag";
            // 
            // lblFriday
            // 
            this.lblFriday.AutoSize = true;
            this.lblFriday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFriday.Location = new System.Drawing.Point(3, 460);
            this.lblFriday.Name = "lblFriday";
            this.lblFriday.Size = new System.Drawing.Size(625, 13);
            this.lblFriday.TabIndex = 15;
            this.lblFriday.Text = "Vrijdag";
            // 
            // lblSaturday
            // 
            this.lblSaturday.AutoSize = true;
            this.lblSaturday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSaturday.Location = new System.Drawing.Point(3, 575);
            this.lblSaturday.Name = "lblSaturday";
            this.lblSaturday.Size = new System.Drawing.Size(625, 13);
            this.lblSaturday.TabIndex = 16;
            this.lblSaturday.Text = "Zaterdag";
            // 
            // lblToPlan
            // 
            this.lblToPlan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblToPlan.AutoSize = true;
            this.lblToPlan.Location = new System.Drawing.Point(-3, 436);
            this.lblToPlan.Name = "lblToPlan";
            this.lblToPlan.Size = new System.Drawing.Size(100, 13);
            this.lblToPlan.TabIndex = 17;
            this.lblToPlan.Text = "Te plannen bonnen";
            // 
            // rootPanel
            // 
            this.rootPanel.BackColor = System.Drawing.SystemColors.Window;
            this.rootPanel.Controls.Add(this.panel3);
            this.rootPanel.Controls.Add(this.panel2);
            this.rootPanel.Controls.Add(this.panel1);
            this.rootPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rootPanel.Location = new System.Drawing.Point(0, 0);
            this.rootPanel.Name = "rootPanel";
            this.rootPanel.Size = new System.Drawing.Size(1450, 732);
            this.rootPanel.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gbFilter);
            this.panel3.Controls.Add(this.tvJobOrders);
            this.panel3.Controls.Add(this.lblToPlan);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.tvSalesOrders);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(854, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(596, 732);
            this.panel3.TabIndex = 33;
            // 
            // gbFilter
            // 
            this.gbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFilter.Controls.Add(this.tbFilter);
            this.gbFilter.Controls.Add(this.btnFilter);
            this.gbFilter.Location = new System.Drawing.Point(0, 18);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(584, 47);
            this.gbFilter.TabIndex = 30;
            this.gbFilter.TabStop = false;
            this.gbFilter.Text = "Filter";
            // 
            // tbFilter
            // 
            this.tbFilter.Location = new System.Drawing.Point(6, 19);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(491, 20);
            this.tbFilter.TabIndex = 28;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(503, 16);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 29;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Verkoopregels";
            // 
            // tvSalesOrders
            // 
            this.tvSalesOrders.AllowDrop = true;
            this.tvSalesOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tvSalesOrders.Location = new System.Drawing.Point(0, 84);
            this.tvSalesOrders.Name = "tvSalesOrders";
            this.tvSalesOrders.Size = new System.Drawing.Size(584, 349);
            this.tvSalesOrders.TabIndex = 26;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblWeek);
            this.panel2.Controls.Add(this.calendar);
            this.panel2.Controls.Add(this.listBox1);
            this.panel2.Controls.Add(this.lblPlannedPeople);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(208, 732);
            this.panel2.TabIndex = 32;
            // 
            // lblWeek
            // 
            this.lblWeek.AutoSize = true;
            this.lblWeek.Location = new System.Drawing.Point(6, 452);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(33, 13);
            this.lblWeek.TabIndex = 21;
            this.lblWeek.Text = "week";
            // 
            // calendar
            // 
            this.calendar.AllowDrop = true;
            this.calendar.BackColor = System.Drawing.SystemColors.Window;
            this.calendar.CalendarDimensions = new System.Drawing.Size(1, 3);
            this.calendar.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.calendar.Location = new System.Drawing.Point(9, 9);
            this.calendar.MaxSelectionCount = 6;
            this.calendar.Name = "calendar";
            this.calendar.ShowToday = false;
            this.calendar.ShowTodayCircle = false;
            this.calendar.ShowWeekNumbers = true;
            this.calendar.TabIndex = 20;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.Enabled = false;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(9, 492);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox1.Size = new System.Drawing.Size(193, 225);
            this.listBox1.TabIndex = 24;
            // 
            // lblPlannedPeople
            // 
            this.lblPlannedPeople.AutoSize = true;
            this.lblPlannedPeople.Location = new System.Drawing.Point(6, 476);
            this.lblPlannedPeople.Name = "lblPlannedPeople";
            this.lblPlannedPeople.Size = new System.Drawing.Size(100, 13);
            this.lblPlannedPeople.TabIndex = 25;
            this.lblPlannedPeople.Text = "Geplande personen";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(208, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(646, 732);
            this.panel1.TabIndex = 31;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblMonday, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lvSaturday, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblSaturday, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lvFriday, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lvThursday, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblThursday, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lvWednesday, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblFriday, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblWednesday, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lvTuesday, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lvMonday, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTuesday, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66666F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(631, 695);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // WeekPlanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1450, 732);
            this.Controls.Add(this.rootPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeekPlanning";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ridder Personeelsplanning";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.rootPanel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.gbFilter.ResumeLayout(false);
            this.gbFilter.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView tvJobOrders;
        private System.Windows.Forms.ListView lvMonday;
        private System.Windows.Forms.ColumnHeader jobOrderDetailWorkActivityCode;
        private System.Windows.Forms.ColumnHeader jobOrderDetailWorkActivityDescription;
        private System.Windows.Forms.ListView lvTuesday;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView lvWednesday;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView lvThursday;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView lvFriday;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ListView lvSaturday;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label lblMonday;
        private System.Windows.Forms.Label lblTuesday;
        private System.Windows.Forms.Label lblWednesday;
        private System.Windows.Forms.Label lblThursday;
        private System.Windows.Forms.Label lblFriday;
        private System.Windows.Forms.Label lblSaturday;
        private System.Windows.Forms.Label lblToPlan;
        private MonthCalendarEx calendar;
        private System.Windows.Forms.Panel rootPanel;
        private System.Windows.Forms.Label lblWeek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPlannedPeople;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TreeView tvSalesOrders;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.GroupBox gbFilter;
        private System.Windows.Forms.ColumnHeader jobOrdeDetailWorkActivityOrderInfo;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}