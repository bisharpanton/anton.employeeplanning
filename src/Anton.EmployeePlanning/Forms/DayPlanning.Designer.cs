﻿namespace Anton.EmployeePlanning.Forms
{
    partial class DayPlanning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DayPlanning));
            this.rootPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEmployees = new System.Windows.Forms.Label();
            this.lvEmployees = new System.Windows.Forms.ListView();
            this.employeeCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.employeeFirstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.employeeNamePrefix = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.employeeLastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvZzp = new System.Windows.Forms.ListView();
            this.zzpFirstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.zzpNamePrefix = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.zzpLastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblZzp = new System.Windows.Forms.Label();
            this.gbWorkActivity = new System.Windows.Forms.GroupBox();
            this.tbHours = new System.Windows.Forms.TextBox();
            this.lblHours = new System.Windows.Forms.Label();
            this.tbWaCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tbWaDescription = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.tbQuantity = new System.Windows.Forms.TextBox();
            this.lblPlannedPeople = new System.Windows.Forms.Label();
            this.tvPlannedPeople = new System.Windows.Forms.TreeView();
            this.gbDetails = new System.Windows.Forms.GroupBox();
            this.lblOrderDescription = new System.Windows.Forms.Label();
            this.lblRelation = new System.Windows.Forms.Label();
            this.tbOrderDescription = new System.Windows.Forms.TextBox();
            this.tbRelation = new System.Windows.Forms.TextBox();
            this.lblJobOrder = new System.Windows.Forms.Label();
            this.tbJobOrder = new System.Windows.Forms.TextBox();
            this.lvJobOrderDetailWorkActivities = new System.Windows.Forms.ListView();
            this.jobOrderDetailWorkActivityCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jobOrderDetailWorkActivityDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rootPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbWorkActivity.SuspendLayout();
            this.gbDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // rootPanel
            // 
            this.rootPanel.BackColor = System.Drawing.SystemColors.Window;
            this.rootPanel.Controls.Add(this.tableLayoutPanel1);
            this.rootPanel.Controls.Add(this.gbWorkActivity);
            this.rootPanel.Controls.Add(this.lblPlannedPeople);
            this.rootPanel.Controls.Add(this.tvPlannedPeople);
            this.rootPanel.Controls.Add(this.gbDetails);
            this.rootPanel.Controls.Add(this.lvJobOrderDetailWorkActivities);
            this.rootPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rootPanel.Location = new System.Drawing.Point(0, 0);
            this.rootPanel.Name = "rootPanel";
            this.rootPanel.Size = new System.Drawing.Size(1122, 673);
            this.rootPanel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblEmployees, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lvEmployees, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lvZzp, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblZzp, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 272);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(271, 389);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // lblEmployees
            // 
            this.lblEmployees.AutoSize = true;
            this.lblEmployees.Location = new System.Drawing.Point(3, 0);
            this.lblEmployees.Name = "lblEmployees";
            this.lblEmployees.Size = new System.Drawing.Size(126, 13);
            this.lblEmployees.TabIndex = 8;
            this.lblEmployees.Text = "Beschikbare werknemers";
            // 
            // lvEmployees
            // 
            this.lvEmployees.AllowDrop = true;
            this.lvEmployees.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.employeeCode,
            this.employeeFirstName,
            this.employeeNamePrefix,
            this.employeeLastName});
            this.lvEmployees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvEmployees.FullRowSelect = true;
            this.lvEmployees.Location = new System.Drawing.Point(3, 16);
            this.lvEmployees.Name = "lvEmployees";
            this.lvEmployees.Size = new System.Drawing.Size(265, 175);
            this.lvEmployees.TabIndex = 5;
            this.lvEmployees.UseCompatibleStateImageBehavior = false;
            this.lvEmployees.View = System.Windows.Forms.View.Details;
            // 
            // employeeCode
            // 
            this.employeeCode.Text = "Code";
            // 
            // employeeFirstName
            // 
            this.employeeFirstName.Text = "Firstname";
            // 
            // employeeNamePrefix
            // 
            this.employeeNamePrefix.Text = "Nameprefix";
            // 
            // employeeLastName
            // 
            this.employeeLastName.Text = "Lastname";
            // 
            // lvZzp
            // 
            this.lvZzp.AllowDrop = true;
            this.lvZzp.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.zzpFirstName,
            this.zzpNamePrefix,
            this.zzpLastName});
            this.lvZzp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvZzp.FullRowSelect = true;
            this.lvZzp.Location = new System.Drawing.Point(3, 210);
            this.lvZzp.Name = "lvZzp";
            this.lvZzp.Size = new System.Drawing.Size(265, 176);
            this.lvZzp.TabIndex = 6;
            this.lvZzp.UseCompatibleStateImageBehavior = false;
            this.lvZzp.View = System.Windows.Forms.View.Details;
            // 
            // zzpFirstName
            // 
            this.zzpFirstName.Text = "Firstname";
            // 
            // zzpNamePrefix
            // 
            this.zzpNamePrefix.Text = "Nameprefix";
            // 
            // zzpLastName
            // 
            this.zzpLastName.Text = "Lastname";
            // 
            // lblZzp
            // 
            this.lblZzp.AutoSize = true;
            this.lblZzp.Location = new System.Drawing.Point(3, 194);
            this.lblZzp.Name = "lblZzp";
            this.lblZzp.Size = new System.Drawing.Size(106, 13);
            this.lblZzp.TabIndex = 7;
            this.lblZzp.Text = "Beschikbare ZZP\'ers";
            // 
            // gbWorkActivity
            // 
            this.gbWorkActivity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbWorkActivity.Controls.Add(this.tbHours);
            this.gbWorkActivity.Controls.Add(this.lblHours);
            this.gbWorkActivity.Controls.Add(this.tbWaCode);
            this.gbWorkActivity.Controls.Add(this.lblCode);
            this.gbWorkActivity.Controls.Add(this.lblDescription);
            this.gbWorkActivity.Controls.Add(this.tbWaDescription);
            this.gbWorkActivity.Controls.Add(this.lblQuantity);
            this.gbWorkActivity.Controls.Add(this.tbQuantity);
            this.gbWorkActivity.Location = new System.Drawing.Point(290, 141);
            this.gbWorkActivity.Name = "gbWorkActivity";
            this.gbWorkActivity.Size = new System.Drawing.Size(820, 128);
            this.gbWorkActivity.TabIndex = 9;
            this.gbWorkActivity.TabStop = false;
            this.gbWorkActivity.Text = "Bewerking";
            // 
            // tbHours
            // 
            this.tbHours.Location = new System.Drawing.Point(332, 48);
            this.tbHours.Name = "tbHours";
            this.tbHours.ReadOnly = true;
            this.tbHours.Size = new System.Drawing.Size(199, 20);
            this.tbHours.TabIndex = 7;
            // 
            // lblHours
            // 
            this.lblHours.AutoSize = true;
            this.lblHours.Location = new System.Drawing.Point(259, 51);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(61, 13);
            this.lblHours.TabIndex = 6;
            this.lblHours.Text = "Aantal uren";
            // 
            // tbWaCode
            // 
            this.tbWaCode.Location = new System.Drawing.Point(110, 22);
            this.tbWaCode.Name = "tbWaCode";
            this.tbWaCode.ReadOnly = true;
            this.tbWaCode.Size = new System.Drawing.Size(143, 20);
            this.tbWaCode.TabIndex = 1;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(10, 25);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(32, 13);
            this.lblCode.TabIndex = 0;
            this.lblCode.Text = "Code";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(259, 25);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(67, 13);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Omschrijving";
            // 
            // tbWaDescription
            // 
            this.tbWaDescription.Location = new System.Drawing.Point(332, 22);
            this.tbWaDescription.Name = "tbWaDescription";
            this.tbWaDescription.ReadOnly = true;
            this.tbWaDescription.Size = new System.Drawing.Size(482, 20);
            this.tbWaDescription.TabIndex = 3;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(10, 51);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(55, 13);
            this.lblQuantity.TabIndex = 4;
            this.lblQuantity.Text = "Aantal M2";
            // 
            // tbQuantity
            // 
            this.tbQuantity.Location = new System.Drawing.Point(110, 48);
            this.tbQuantity.Name = "tbQuantity";
            this.tbQuantity.ReadOnly = true;
            this.tbQuantity.Size = new System.Drawing.Size(143, 20);
            this.tbQuantity.TabIndex = 5;
            // 
            // lblPlannedPeople
            // 
            this.lblPlannedPeople.AutoSize = true;
            this.lblPlannedPeople.Location = new System.Drawing.Point(297, 272);
            this.lblPlannedPeople.Name = "lblPlannedPeople";
            this.lblPlannedPeople.Size = new System.Drawing.Size(100, 13);
            this.lblPlannedPeople.TabIndex = 4;
            this.lblPlannedPeople.Text = "Geplande personen";
            // 
            // tvPlannedPeople
            // 
            this.tvPlannedPeople.AllowDrop = true;
            this.tvPlannedPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvPlannedPeople.Location = new System.Drawing.Point(290, 288);
            this.tvPlannedPeople.Name = "tvPlannedPeople";
            this.tvPlannedPeople.Size = new System.Drawing.Size(820, 370);
            this.tvPlannedPeople.TabIndex = 3;
            // 
            // gbDetails
            // 
            this.gbDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDetails.Controls.Add(this.lblOrderDescription);
            this.gbDetails.Controls.Add(this.lblRelation);
            this.gbDetails.Controls.Add(this.tbOrderDescription);
            this.gbDetails.Controls.Add(this.tbRelation);
            this.gbDetails.Controls.Add(this.lblJobOrder);
            this.gbDetails.Controls.Add(this.tbJobOrder);
            this.gbDetails.Location = new System.Drawing.Point(290, 13);
            this.gbDetails.Name = "gbDetails";
            this.gbDetails.Size = new System.Drawing.Size(820, 122);
            this.gbDetails.TabIndex = 2;
            this.gbDetails.TabStop = false;
            this.gbDetails.Text = "Details";
            // 
            // lblOrderDescription
            // 
            this.lblOrderDescription.AutoSize = true;
            this.lblOrderDescription.Location = new System.Drawing.Point(10, 73);
            this.lblOrderDescription.Name = "lblOrderDescription";
            this.lblOrderDescription.Size = new System.Drawing.Size(94, 13);
            this.lblOrderDescription.TabIndex = 11;
            this.lblOrderDescription.Text = "Order omschrijving";
            // 
            // lblRelation
            // 
            this.lblRelation.AutoSize = true;
            this.lblRelation.Location = new System.Drawing.Point(10, 47);
            this.lblRelation.Name = "lblRelation";
            this.lblRelation.Size = new System.Drawing.Size(40, 13);
            this.lblRelation.TabIndex = 10;
            this.lblRelation.Text = "Relatie";
            // 
            // tbOrderDescription
            // 
            this.tbOrderDescription.Location = new System.Drawing.Point(110, 70);
            this.tbOrderDescription.Name = "tbOrderDescription";
            this.tbOrderDescription.ReadOnly = true;
            this.tbOrderDescription.Size = new System.Drawing.Size(704, 20);
            this.tbOrderDescription.TabIndex = 9;
            // 
            // tbRelation
            // 
            this.tbRelation.Location = new System.Drawing.Point(110, 44);
            this.tbRelation.Name = "tbRelation";
            this.tbRelation.ReadOnly = true;
            this.tbRelation.Size = new System.Drawing.Size(704, 20);
            this.tbRelation.TabIndex = 8;
            // 
            // lblJobOrder
            // 
            this.lblJobOrder.AutoSize = true;
            this.lblJobOrder.Location = new System.Drawing.Point(10, 21);
            this.lblJobOrder.Name = "lblJobOrder";
            this.lblJobOrder.Size = new System.Drawing.Size(26, 13);
            this.lblJobOrder.TabIndex = 7;
            this.lblJobOrder.Text = "Bon";
            // 
            // tbJobOrder
            // 
            this.tbJobOrder.Location = new System.Drawing.Point(110, 18);
            this.tbJobOrder.Name = "tbJobOrder";
            this.tbJobOrder.ReadOnly = true;
            this.tbJobOrder.Size = new System.Drawing.Size(143, 20);
            this.tbJobOrder.TabIndex = 6;
            // 
            // lvJobOrderDetailWorkActivities
            // 
            this.lvJobOrderDetailWorkActivities.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.jobOrderDetailWorkActivityCode,
            this.jobOrderDetailWorkActivityDescription});
            this.lvJobOrderDetailWorkActivities.FullRowSelect = true;
            this.lvJobOrderDetailWorkActivities.HideSelection = false;
            this.lvJobOrderDetailWorkActivities.Location = new System.Drawing.Point(15, 12);
            this.lvJobOrderDetailWorkActivities.MultiSelect = false;
            this.lvJobOrderDetailWorkActivities.Name = "lvJobOrderDetailWorkActivities";
            this.lvJobOrderDetailWorkActivities.Size = new System.Drawing.Size(265, 257);
            this.lvJobOrderDetailWorkActivities.TabIndex = 1;
            this.lvJobOrderDetailWorkActivities.UseCompatibleStateImageBehavior = false;
            this.lvJobOrderDetailWorkActivities.View = System.Windows.Forms.View.Details;
            // 
            // jobOrderDetailWorkActivityCode
            // 
            this.jobOrderDetailWorkActivityCode.Text = "Code";
            this.jobOrderDetailWorkActivityCode.Width = 80;
            // 
            // jobOrderDetailWorkActivityDescription
            // 
            this.jobOrderDetailWorkActivityDescription.Text = "Description";
            this.jobOrderDetailWorkActivityDescription.Width = 139;
            // 
            // DayPlanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 673);
            this.Controls.Add(this.rootPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DayPlanning";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ridder Personeelsplanning";
            this.rootPanel.ResumeLayout(false);
            this.rootPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.gbWorkActivity.ResumeLayout(false);
            this.gbWorkActivity.PerformLayout();
            this.gbDetails.ResumeLayout(false);
            this.gbDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel rootPanel;
        private System.Windows.Forms.ListView lvJobOrderDetailWorkActivities;
        private System.Windows.Forms.ColumnHeader jobOrderDetailWorkActivityCode;
        private System.Windows.Forms.ColumnHeader jobOrderDetailWorkActivityDescription;
        private System.Windows.Forms.GroupBox gbDetails;
        private System.Windows.Forms.TextBox tbWaCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox tbWaDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblPlannedPeople;
        private System.Windows.Forms.TreeView tvPlannedPeople;
        private System.Windows.Forms.Label lblEmployees;
        private System.Windows.Forms.Label lblZzp;
        private System.Windows.Forms.ListView lvZzp;
        private System.Windows.Forms.ListView lvEmployees;
        private System.Windows.Forms.ColumnHeader employeeCode;
        private System.Windows.Forms.ColumnHeader employeeFirstName;
        private System.Windows.Forms.ColumnHeader employeeNamePrefix;
        private System.Windows.Forms.ColumnHeader employeeLastName;
        private System.Windows.Forms.ColumnHeader zzpFirstName;
        private System.Windows.Forms.ColumnHeader zzpNamePrefix;
        private System.Windows.Forms.ColumnHeader zzpLastName;
        private System.Windows.Forms.TextBox tbQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.GroupBox gbWorkActivity;
        private System.Windows.Forms.Label lblOrderDescription;
        private System.Windows.Forms.Label lblRelation;
        private System.Windows.Forms.TextBox tbOrderDescription;
        private System.Windows.Forms.TextBox tbRelation;
        private System.Windows.Forms.Label lblJobOrder;
        private System.Windows.Forms.TextBox tbJobOrder;
        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.TextBox tbHours;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}