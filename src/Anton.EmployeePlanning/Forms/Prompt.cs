﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anton.EmployeePlanning.Forms
{
    public partial class Prompt : Form
    {
        public bool Done { get; set; }

        public double Quantity { get; set; }

        public Prompt(string caption, double quantity)
        {
            InitializeComponent();
            Text = caption;
            numQuantity.Value = (decimal)quantity;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Done = cbDone.Checked;
            Quantity = (double)numQuantity.Value;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
