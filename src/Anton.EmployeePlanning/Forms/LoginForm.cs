﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anton.EmployeePlanning.CredentialManagement;
using Anton.EmployeePlanning.Helpers;
using Anton.EmployeePlanning.Properties;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;
using Ridder.Common;
using ProgressBarStyle = System.Windows.Forms.ProgressBarStyle;

namespace Anton.EmployeePlanning.Forms
{
    public partial class LoginForm : Form
    {
        private readonly BusyHelper _busy;
        private readonly BindingList<LoginCompany> _companies = new BindingList<LoginCompany>();
        private readonly BindingList<LoginCompanyUser> _users = new BindingList<LoginCompanyUser>();
        private const string CredentialManagerTarget = "RidderEmployeePlanning";

        public LoginForm()
        {
            AskIQClientPath = false;

            var oldCursor = Cursor;
            _busy = BusyHelper.Create(
                this,
                f => {
                    btnLogin.Visible = false;
                    users.Enabled = false;
                    administrations.Enabled = false;
                    txtPassword.Enabled = false;

                    progressBar1.Style = ProgressBarStyle.Marquee;
                    progressBar1.MarqueeAnimationSpeed = 100;
                    progressBar1.Visible = true;

                    oldCursor = Cursor;
                    Application.UseWaitCursor = true;
                },
                f => {
                    users.Enabled = true;
                    administrations.Enabled = true;
                    txtPassword.Enabled = true;
                    btnLogin.Visible = true;

                    progressBar1.MarqueeAnimationSpeed = 0;
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    progressBar1.Value = progressBar1.Minimum;
                    progressBar1.Visible = false;

                    Application.UseWaitCursor = false;
                    Cursor = oldCursor;
                });

            IQClientPath = Settings.Default.SDK_ClientPath;
            CompanyName = Settings.Default.SDK_CompanyName;
            Username = Settings.Default.SDK_UserName;
            Password = GetPassword();

            InitializeComponent();

            // init bindings
            administrations.ValueMember = "CompanyName";
            administrations.DisplayMember = "CompanyName";
            administrations.DataSource = _companies;

            users.ValueMember = "UserName";
            users.DisplayMember = "UserName";
            users.DataSource = _users;
        }

        public bool AskIQClientPath { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public new string CompanyName { get; set; }

        public SdkSession Sdk { get; set; }

        public string IQClientPath { get; set; }

        protected override async void OnLoad(EventArgs e)
        {
            users.Text = Username;
            administrations.Text = CompanyName;
            txtPassword.Text = Password;

            if (AskIQClientPath && (string.IsNullOrEmpty(IQClientPath) || !Directory.Exists(IQClientPath)))
            {
                ChooseRidderIQClientPath();
            }

            using (_busy.Busy())
            {
                await LoadAdministrations();
            }

            if (!string.IsNullOrEmpty(Username))
            {
                users.SelectedValue = Username;
                if (!string.IsNullOrEmpty(Password))
                {
                    this.txtPassword.Text = Password;
                    btnLogin.Select();
                }
                else
                {
                    txtPassword.Select();
                }
            }
            else
            {
                users.Select();
            }

            base.OnLoad(e);
        }

        private void administrations_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUsers(_companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            users.Select();

            UpdateSettings();
        }

        private bool ChooseRidderIQClientPath()
        {
            using (var sf = new OpenFileDialog())
            {
                if (!string.IsNullOrEmpty(IQClientPath)
                    && Directory.Exists(IQClientPath))
                {
                    sf.InitialDirectory = IQClientPath;
                }

                sf.CheckFileExists = true;
                sf.FileName = "Client.exe";
                sf.Filter = "Ridder IQ Client|Client.exe";
                sf.CheckPathExists = true;

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    IQClientPath = Path.GetDirectoryName(sf.FileName);
                    UpdateSettings();
                    return true;
                }
            }

            return false;
        }

        private Task<LoginCompany[]> GetAdministrationsAsync()
        {
            return Task.Run(
                () => {
                    var companies = new LoginCompany[] { };
                    var sdk = new RidderIQSDK();

                    if (!string.IsNullOrEmpty(IQClientPath))
                    {
                        sdk.SetRidderIQClientPath(IQClientPath);
                    }

                    companies = sdk.GetAdministrations();
                    return companies;
                });
        }

        private async Task LoadAdministrations()
        {
            try
            {
                var companies = await GetAdministrationsAsync();
                LoadAdministrations(companies);
            }
            catch (SdkException ex)
            {
                MessageBox.Show(
                    ex.Result.GetResult(),
                    "SDK Fout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Onbekende fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadAdministrations(LoginCompany[] companies)
        {
            if (companies.Length == 0)
            {
                return;
            }

            _companies.RaiseListChangedEvents = false;
            _companies.Clear();

            foreach (var company in companies)
            {
                _companies.Add(company);
            }

            _companies.RaiseListChangedEvents = true;
            _companies.ResetBindings();

            if (!string.IsNullOrEmpty(CompanyName)
                && _companies.Any(x => x.CompanyName == CompanyName))
            {
                administrations.SelectedValue = CompanyName;
                LoadUsers(companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            }
            else if (companies.Length > 0)
            {
                administrations.SelectedIndex = 0;
                LoadUsers(companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            }
        }

        private void LoadUsers(LoginCompany company)
        {
            if (company == null)
            {
                _users.Clear();
                return;
            }

            _users.RaiseListChangedEvents = false;
            _users.Clear();
            foreach (var user in company.Users)
            {
                _users.Add(user);
            }

            _users.RaiseListChangedEvents = true;
            _users.ResetBindings();

            if (!string.IsNullOrEmpty(Username)
                && _users.Any(x => Username.Equals(x.UserName, StringComparison.OrdinalIgnoreCase)))
            {
                users.SelectedValue = Username;
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            var username = users.Text;
            var password = this.txtPassword.Text;
            var companyName = administrations.Text;

            bool success = false;
            using (_busy.Busy())
            {
                try
                {
                    Sdk = await Task.Run(
                        () => {
                            // try login
                            var session =
                                new SdkSession(
                                    new SdkConfiguration() {
                                        UserName = username,
                                        Password = password,
                                        CompanyName = companyName,
                                        RidderIQClientPath = IQClientPath
                                    });

                            session.Login();
                            return session;
                        }).ConfigureAwait(true);
                    success = Sdk != null;
                }
                catch (SdkException ex)
                {
                    MessageBox.Show(
                        this,
                        ex.Result.GetResult(),
                        "Ridder Personeelsplanning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Ridder Personeelsplanning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (!success)
            {
                MessageBox.Show(this, "Fout bij inloggen.", "Ridder Personeelsplanning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Enabled = false;
            try
            {
                UpdateSettings();
                SavePassword();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Enabled = true;
            }
        }

        private void UpdateSettings()
        {
            // save settings
            Username = users.Text;
            CompanyName = administrations.Text;
            Password = txtPassword.Text;

            Settings.Default.SDK_ClientPath = IQClientPath;
            Settings.Default.SDK_CompanyName = CompanyName;
            Settings.Default.SDK_UserName = Username;
            Settings.Default.Save();
        }

        private void SavePassword()
        {
            var networkCredentials = new NetworkCredential(Username, Password);
            CredentialManager.SaveCredentials($"{CredentialManagerTarget}{Username}", networkCredentials);
        }

        private string GetPassword()
        {
            var credentials = CredentialManager.GetCredentials($"{CredentialManagerTarget}{Username}");
            if (credentials != null)
            {
                return credentials.Password;
            }
            return "";
        }

        private void users_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPassword.SelectAll();
            txtPassword.Select();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (ChooseRidderIQClientPath())
            {
                using (_busy.Busy())
                {
                    await LoadAdministrations();
                }
            }
        }

        private void administrations_Leave(object sender, EventArgs e)
        {
            UpdateSettings();
        }

        private void users_Leave(object sender, EventArgs e)
        {
            UpdateSettings();
        }
    }
}
