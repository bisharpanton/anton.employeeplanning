﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anton.EmployeePlanning.CalendarEx;
using Anton.EmployeePlanning.Helpers;
using Anton.EmployeePlanning.Models;
using Ridder.Client.SDK.Extensions;

// ReSharper disable InconsistentNaming

namespace Anton.EmployeePlanning.Forms
{
    public partial class WeekPlanning : Form
    {
        private List<DetailsToPlan> AllDetailsToPlan;
        private List<PlannedDaysPerJobOrder> AllPlannedJobOrderDetailWorkActivities;

        private SdkSession _sdk;

        public WeekPlanning()
        {
            InitializeComponent();
            SetCalendarWeek(DateTime.Now.Date);
            Load += OnLoad;
            Shown += OnShown;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            tbFilter.KeyUp += tbFilter_KeyUp;

            calendar.DragDropDate += Calendar_DragDrop;
            calendar.DragOverArea += Calendar_DragOverArea;
            calendar.DateSelected += Calendar_DateSelected;

            tvJobOrders.DragDrop += TreeView1_DragDrop;
            tvJobOrders.ItemDrag += TreeView1_ItemDrag;
            tvJobOrders.DragOver += TreeView1_DragOver;

            tvSalesOrders.ItemDrag += TreeView1_ItemDrag;
            tvSalesOrders.DragOver += TreeViewSales_DragOver;

            lvMonday.DragDrop += ListView_DragDrop;
            lvTuesday.DragDrop += ListView_DragDrop;
            lvWednesday.DragDrop += ListView_DragDrop;
            lvThursday.DragDrop += ListView_DragDrop;
            lvFriday.DragDrop += ListView_DragDrop;
            lvSaturday.DragDrop += ListView_DragDrop;

            lvMonday.DoubleClick += ListView_DoubleClick;
            lvTuesday.DoubleClick += ListView_DoubleClick;
            lvWednesday.DoubleClick += ListView_DoubleClick;
            lvThursday.DoubleClick += ListView_DoubleClick;
            lvFriday.DoubleClick += ListView_DoubleClick;
            lvSaturday.DoubleClick += ListView_DoubleClick;

            lvMonday.SelectedIndexChanged += ListView_IndexChanged;
            lvTuesday.SelectedIndexChanged += ListView_IndexChanged;
            lvWednesday.SelectedIndexChanged += ListView_IndexChanged;
            lvThursday.SelectedIndexChanged += ListView_IndexChanged;
            lvFriday.SelectedIndexChanged += ListView_IndexChanged;
            lvSaturday.SelectedIndexChanged += ListView_IndexChanged;
        }

        private void tbFilter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnFilter.PerformClick();
            }
        }

        private void ListView_IndexChanged(object sender, EventArgs e)
        {
            var lv = (ListView)sender;
            var personNames = new List<string>();

            if (lv.SelectedIndices.Count > 0)
            {
                var plannedJobOrder = (PlannedDaysPerJobOrder)lv.SelectedItems[0].Tag;

                if (plannedJobOrder.Employees != null)
                {
                    var employees = plannedJobOrder.Employees.Where(x => x.FK_EMPLOYEE.HasValue).Select(x => x.Employee.Person).ToList();
                    var contacts = plannedJobOrder.Employees.Where(x => x.FK_CONTACT.HasValue).Select(x => x.Contact.Person).ToList();
                    var persons = employees.Concat(contacts);

                    personNames = persons.Select(x => $"{x.FIRSTNAME} {x.NAMEPREFIX}{(string.IsNullOrEmpty(x.NAMEPREFIX) ? "" : " ")}{x.LASTNAME}").ToList();
                }
            }

            listBox1.DataSource = personNames;
        }

        private async void OnShown(object sender, EventArgs eventArgs)
        {
            rootPanel.Enabled = false;
            UseWaitCursor = true;

            using (var loginDialog = new LoginForm())
            {
                var result = loginDialog.ShowDialog();
                if (result != DialogResult.OK)
                {
                    Application.Exit();
                    return;
                }
                _sdk = loginDialog.Sdk;
            }

            await RefreshData();

            rootPanel.Enabled = true;
            UseWaitCursor = false;
        }

        private void ListView_DoubleClick(object sender, EventArgs e)
        {
            var sourceLv = (ListView)sender;
            var dt = (DateTime)sourceLv.Tag;
            if (sourceLv.SelectedIndices.Count == 0)
            {
                return;
            }
            var selected = ((PlannedDaysPerJobOrder)sourceLv.SelectedItems[0].Tag).Id;
            rootPanel.Enabled = false;
            var dayPlanning = new DayPlanning(_sdk, dt, selected);
            dayPlanning.Show(this);
            dayPlanning.Activate();
        }

        private async Task RefreshData()
        {
            await Task.Run(() => {
                    AllDetailsToPlan = _sdk.Find<DetailsToPlan>().ToList();
                    AllPlannedJobOrderDetailWorkActivities = _sdk.Find<PlannedDaysPerJobOrder>(x => x.DATE >= calendar.SelectionStart && x.DATE < calendar.SelectionEnd.Date.AddDays(1)).ToList();
                });

            InitializeTreeViews();
            InitializeListViews();
        }

        private void InitializeTreeViews(string filter = "")
        {
            var jobOrdersExpansionState = tvJobOrders.Nodes.GetExpansionState();
            var salesOrdersExpansionState = tvSalesOrders.Nodes.GetExpansionState();
            tvJobOrders.Nodes.Clear();
            tvSalesOrders.Nodes.Clear();

            var jobOrderDetailsToPlan = AllDetailsToPlan.Where(x => x.FK_JOBORDERDETAILWORKACTIVITY.HasValue && x.JobOrderDetailWorkActivity.WorkActivity.PLANABLE).ToList();
            var filtered = jobOrderDetailsToPlan.Where(
                    x => x.JobOrderDetailWorkActivity.JobOrder.Order.ORDERNUMBER.ToString().IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0
                         || x.JobOrderDetailWorkActivity.JobOrder.Order.Relation.NAME.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0
                         || x.JobOrderDetailWorkActivity.DESCRIPTION.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0)
                .ToList();
            var jobOrders = filtered.GroupBy(x => x.JobOrderDetailWorkActivity.JobOrder);
            foreach (var jobOrder in jobOrders)
            {
                var nodeDescription = $"{jobOrder.Key.Order.ORDERNUMBER}.{jobOrder.Key.JOBORDERNUMBER} - {jobOrder.Key.Order.Relation.NAME} - {jobOrder.Key.Order.DESCRIPTION} - {jobOrder.Key.DESCRIPTION}";
                var jobOrderNode = new TreeNode(nodeDescription)
                {
                    Tag = jobOrder.Key
                };
                jobOrderNode.ToolTipText = nodeDescription;
                foreach (var detail in jobOrder)
                {
                    jobOrderNode.Nodes.Add(detail.ToTreeNode());
                }

                tvJobOrders.ShowNodeToolTips = true;
                tvJobOrders.Nodes.Add(jobOrderNode);
            }

            var salesOrdersToPlan = AllDetailsToPlan.Where(x => x.FK_SALESORDERDETAILASSEMBLY.HasValue);
            foreach (var salesOrder in salesOrdersToPlan)
            {
                var nodeDescription = $"{salesOrder.SalesOrderDetailAssembly.Order.ORDERNUMBER}.{salesOrder.SalesOrderDetailAssembly.LINENUMBER} - {salesOrder.SalesOrderDetailAssembly.Order.Relation.NAME} - {salesOrder.SalesOrderDetailAssembly.Order.DESCRIPTION} - {salesOrder.SalesOrderDetailAssembly.DESCRIPTION}";
                var jobOrderNode = new TreeNode(nodeDescription)
                {
                    Tag = salesOrder//.SalesOrderDetailAssembly
                };
                jobOrderNode.ToolTipText = nodeDescription;
                tvSalesOrders.ShowNodeToolTips = true;
                tvSalesOrders.Nodes.Add(jobOrderNode);
            }

            tvJobOrders.Nodes.SetExpansionState(jobOrdersExpansionState);
            tvSalesOrders.Nodes.SetExpansionState(salesOrdersExpansionState);
        }

        private void InitializeListViews()
        {
            lvMonday.Items.Clear();
            lvTuesday.Items.Clear();
            lvWednesday.Items.Clear();
            lvThursday.Items.Clear();
            lvFriday.Items.Clear();
            lvSaturday.Items.Clear();

            var plannedByDay = AllPlannedJobOrderDetailWorkActivities.Where(x => x.DATE.HasValue).GroupBy(x => x.DATE.Value);
            foreach (var day in plannedByDay)
            {
                var dayOfWeek = day.Key.DayOfWeek;
                foreach (var plannedJobOrders in day)
                {
                    var listViewItem = plannedJobOrders.ToListViewItem();
                    switch (dayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            lvMonday.Items.Add(listViewItem);
                            break;
                        case DayOfWeek.Tuesday:
                            lvTuesday.Items.Add(listViewItem);
                            break;
                        case DayOfWeek.Wednesday:
                            lvWednesday.Items.Add(listViewItem);
                            break;
                        case DayOfWeek.Thursday:
                            lvThursday.Items.Add(listViewItem);
                            break;
                        case DayOfWeek.Friday:
                            lvFriday.Items.Add(listViewItem);
                            break;
                        case DayOfWeek.Saturday:
                            lvSaturday.Items.Add(listViewItem);
                            break;
                    }
                }
            }

            lvMonday.ShowItemToolTips = true;
            lvTuesday.ShowItemToolTips = true;
            lvWednesday.ShowItemToolTips = true;
            lvThursday.ShowItemToolTips = true;
            lvFriday.ShowItemToolTips = true;
            lvSaturday.ShowItemToolTips = true;
        }

        private async Task PlanJobOrderDetailWorkActivity(PlannedDaysPerJobOrder pdpjo, bool jobOrderWorkActivityPlanned)
        {
            await Task.Run(
                () =>
                    {
                        if (jobOrderWorkActivityPlanned)
                        {
                            pdpjo.JobOrderDetailWorkActivity.PLANNINGDONE = true;
                            _sdk.Update(pdpjo.JobOrderDetailWorkActivity);
                        }
                        _sdk.Insert(pdpjo);
                    });
        }

        private async Task UnPlanJobOrderDetailWorkActivity(PlannedDaysPerJobOrder pdpjo)
        {
            await Task.Run(
                () =>
                    {
                        var jodwa = pdpjo.JobOrderDetailWorkActivity;
                        _sdk.Delete(pdpjo);
                        var jodwas = _sdk.Find<PlannedDaysPerJobOrder>(x => x.FK_JOBORDERDETAILWORKACTIVITY == jodwa.Id).ToList();
                        if (jodwas.Count == 0)
                        {
                            jodwa.PLANNINGDONE = false;
                            _sdk.Update(jodwa);
                        }
                    });
        }

        private async void TreeView1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                var data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                if (!(data.Tag is PlannedDaysPerJobOrder pdpjo))
                {
                    return;
                }
                await UnPlanJobOrderDetailWorkActivity(pdpjo);
            }
            else if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                var treeNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
                if (treeNode.Tag is DetailsToPlan dtp && dtp.SalesOrderDetailAssembly != null)
                {
                    var quantity = dtp.SalesOrderDetailAssembly.QUANTITY - GetQuantityInProduction(dtp.SalesOrderDetailAssembly.Id);
                    bool done;
                    using (var prompt = new Prompt(dtp.SalesOrderDetailAssembly.DESCRIPTION, quantity))
                    {
                        prompt.ShowDialog();
                        if (prompt.DialogResult != DialogResult.OK)
                        {
                            return;
                        }

                        quantity = prompt.Quantity;
                        done = prompt.Done;
                    }
                    // Onderstaande Guid is het id van de workflow Genereer bon op de verkoopregel stuklijst tabel
                    try
                    {
                        var result = _sdk.Sdk.ExecuteWorkflowEvent("R_SALESORDERDETAILASSEMBLY", dtp.SalesOrderDetailAssembly.Id, new Guid("93c157b5-8ee1-49ce-af7a-930c0738c8df"), new Dictionary<string, object> { { "Aantal", quantity }, { "OutdeliveryComplete", done } });
                        if (result.HasError)
                        {
                            throw new SdkException(result);
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show($@"Bij het genereren van de bon(nen) is de volgende fout opgetreden:{Environment.NewLine}{exception}");
                    }
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            await RefreshData();
        }

        private double GetQuantityInProduction(int salesOrderDetailAssemblyId)
        {
            var orderBase = _sdk.GetRecordsetColumns("R_ORDERBASE", "QUANTITY", $"FK_SALESORDERDETAILASSEMBLYCUSTOM = {salesOrderDetailAssemblyId}").DataTable.AsEnumerable();
            return orderBase.Sum(x => x.Field<double>("QUANTITY"));
        }

        private async void ListView_DragDrop(object sender, DragEventArgs e)
        {
            var sourceLv = (ListView)sender;
            PlannedDaysPerJobOrder pdpjo;
            var planningDone = false;
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                var data = (TreeNode)e.Data.GetData(typeof(TreeNode));
                pdpjo = PlanningDragDropTreeNode(data, e, (DateTime)sourceLv.Tag);
                if (e.Effect != DragDropEffects.Copy)
                {
                    planningDone = true;
                }
            }
            else if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                var data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                if (data.ListView == sourceLv)
                {
                    return;
                }
                pdpjo = await PlanningDragDropListViewItem(data, e, (DateTime)sourceLv.Tag);
            }
            else
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            if (pdpjo == null)
            {
                return;
            }
            await PlanJobOrderDetailWorkActivity(pdpjo, planningDone);
            await RefreshData();
        }

        private static PlannedDaysPerJobOrder PlanningDragDropTreeNode(TreeNode data, DragEventArgs e, DateTime dt)
        {
            if (!(data.Tag is DetailsToPlan dtp))
            {
                return null;
            }
            return new PlannedDaysPerJobOrder
            {
                FK_JOBORDERDETAILWORKACTIVITY = dtp.FK_JOBORDERDETAILWORKACTIVITY,
                JobOrderDetailWorkActivity = dtp.JobOrderDetailWorkActivity,
                DATE = dt
            };
        }

        private async Task<PlannedDaysPerJobOrder> PlanningDragDropListViewItem(ListViewItem data, DragEventArgs e, DateTime dt)
        {
            if (!(data.Tag is PlannedDaysPerJobOrder existing))
            {
                return null;
            }
            if (e.Effect == DragDropEffects.Copy)
            {
                return new PlannedDaysPerJobOrder
                {
                    FK_JOBORDERDETAILWORKACTIVITY = existing.FK_JOBORDERDETAILWORKACTIVITY,
                    DATE = dt
                };
            }
            else
            {
                var pdpjo = existing;
                await UnPlanJobOrderDetailWorkActivity(pdpjo);
                pdpjo.DATE = dt;
                return pdpjo;
            }
        }

        private void ListView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void TreeView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (!(e.Item is TreeNode node) || !(node.Tag is JobOrderDetailWorkActivity || node.Tag is AssemblyDetailWorkActivity || node.Tag is DetailsToPlan))
            {
                return;
            }

            var effects = DragDropEffects.Move;
            if (node.Tag is DetailsToPlan dtp && dtp.SalesOrderDetailAssembly == null)
            {
                effects = DragDropEffects.Move | DragDropEffects.Copy;
            }
            DoDragDrop(node, effects);
        }

        private async void Calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (!(sender is MonthCalendar localCalendar))
            {
                return;
            }
            SetCalendarWeek(localCalendar.SelectionStart);
            await RefreshData();
        }

        private void SetCalendarWeek(DateTime dt)
        {
            var startDate = dt;
            var monday = startDate.AddDays(1 - (int)startDate.DayOfWeek);
            var tuesday = monday.AddDays(1);
            var wednesday = tuesday.AddDays(1);
            var thursday = wednesday.AddDays(1);
            var friday = thursday.AddDays(1);
            var saturday = friday.AddDays(1);

            calendar.SelectionStart = monday;
            calendar.SelectionEnd = saturday;
            lblWeek.Text = $@"{monday:dd-MM-yyyy} t/m {saturday:dd-MM-yyyy}";

            lvMonday.Tag = monday;
            lblMonday.Text = $@"Maandag ({monday:dd-MM-yyyy})";

            lvTuesday.Tag = tuesday;
            lblTuesday.Text = $@"Dinsdag ({tuesday:dd-MM-yyyy})";

            lvWednesday.Tag = wednesday;
            lblWednesday.Text = $@"Woensdag ({wednesday:dd-MM-yyyy})";

            lvThursday.Tag = thursday;
            lblThursday.Text = $@"Donderdag ({thursday:dd-MM-yyyy})";

            lvFriday.Tag = friday;
            lblFriday.Text = $@"Vrijdag ({friday:dd-MM-yyyy})";

            lvSaturday.Tag = saturday;
            lblSaturday.Text = $@"Zaterdag ({saturday:dd-MM-yyyy})";
        }

        private void Calendar_DragOverArea(object sender, DragDateEventArgs e)
        {
            if (e.Time.HasValue)
            {
                e.Effect = ModifierKeys == Keys.Control ? DragDropEffects.Copy : DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private async void Calendar_DragDrop(object sender, DragDateEventArgs e)
        {
            if (!e.Time.HasValue)
            {
                return;
            }

            PlannedDaysPerJobOrder pdpjo;
            var planningDone = false;
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                var data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
                pdpjo = await PlanningDragDropListViewItem(data, e, e.Time.Value);
            }
            else if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                var data = (TreeNode)e.Data.GetData(typeof(TreeNode));
                pdpjo = PlanningDragDropTreeNode(data, e, e.Time.Value);
                if (e.Effect != DragDropEffects.Copy)
                {
                    planningDone = true;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            /*
            if (data.Tag is AssemblyDetailWorkActivity)
            {
                var adwa = data.Tag as AssemblyDetailWorkActivity;
                // TODO: Workflow aanroepen om verkoopregel stuklijst om te zetten
                // Vervolgens de bonregel bewerking zoeken die overeenkomt met de gesleepte stuklijstregel bewerking en die op de dag plannen waar die heen gesleept wordt

                // Refresh van data?
                return;
            }
            */

            //var jodwa = (JobOrderDetailWorkActivity)data.Tag;

            await PlanJobOrderDetailWorkActivity(pdpjo, planningDone);
            await RefreshData();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED to fix flicker of controls
                return cp;
            }
        }

        private void TreeView1_DragOver(object sender, DragEventArgs e)
        {
            var effect = ModifierKeys == Keys.Control ? DragDropEffects.Copy : DragDropEffects.Move;
            e.Effect = effect;
        }

        private void TreeViewSales_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                var treeNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
                if (treeNode.Tag is DetailsToPlan dtp && dtp.SalesOrderDetailAssembly != null)
                {
                    e.Effect = DragDropEffects.Move;
                    return;
                }
            }
            e.Effect = DragDropEffects.None;
        }

        private void ListView_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                var treeNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
                if (treeNode.Tag is DetailsToPlan dtp && dtp.SalesOrderDetailAssembly != null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
            }

            var effect = ModifierKeys == Keys.Control ? DragDropEffects.Copy : DragDropEffects.Move;
            e.Effect = effect;
        }

        public async Task EnableAll()
        {
            await RefreshData();
            rootPanel.Enabled = true;
            Activate();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var filterString = tbFilter.Text;
            InitializeTreeViews(filterString);
            InitializeListViews();
        }
    }
}
