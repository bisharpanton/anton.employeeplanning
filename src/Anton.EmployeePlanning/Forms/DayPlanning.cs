﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anton.EmployeePlanning.Helpers;
using Anton.EmployeePlanning.Models;
using Ridder.Client.SDK.Extensions;

namespace Anton.EmployeePlanning.Forms
{
    public partial class DayPlanning : Form
    {
        private DateTime _planningDate;

        private readonly SdkSession _sdk;

        private List<PlannedDaysPerJobOrder> _plannedWorkActivities;

        private List<PlannedEmployeesPerDate> _plannedEmployees;

        private List<Employee> _allowedEmployees;

        private List<Contact> _allowedContacts;

        private PlannedDaysPerJobOrder _selectedPdpjo;

        private readonly int _initialSelected;

        public DayPlanning(SdkSession sdk, DateTime dt, int selectedWa)
        {
            InitializeComponent();

            _planningDate = dt;
            _sdk = sdk;
            _initialSelected = selectedWa;

            Load += OnLoad;
            Shown += OnShown;
            Closed += OnClosed;
        }

        private async void OnClosed(object sender, EventArgs e)
        {
            await ((WeekPlanning)Owner).EnableAll();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            // Events binden
            lvJobOrderDetailWorkActivities.SelectedIndexChanged += LvJobOrderDetailWorkActivitiesSelectedIndexChanged;

            tvPlannedPeople.DragDrop += PlannedPeopleDragDrop;
            tvPlannedPeople.DragOver += PlannedPeopleDragOver;
            tvPlannedPeople.ItemDrag += PlannedPeopleItemDrag;
            lvEmployees.ItemDrag += ListViewItemDrag;
            lvEmployees.DragOver += ListViewDragOver;
            lvEmployees.DragDrop += ListViewDragDrop;
            lvZzp.ItemDrag += ListViewItemDrag;
            lvZzp.DragDrop += ListViewDragDrop;
            lvZzp.DragOver += ListViewDragOver;
        }

        private async void ListViewDragDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(TreeNode)))
            {
                return;
            }
            var node = (TreeNode)e.Data.GetData(typeof(TreeNode));
            var pepd = (PlannedEmployeesPerDate)node.Tag;

            await Task.Run(
                () =>
                    {
                        _sdk.Delete(pepd);
                    });

            await RefreshData();
        }

        private void PlannedPeopleItemDrag(object sender, ItemDragEventArgs e)
        {
            var lvi = e.Item as TreeNode;
            if (lvi?.Tag is PlannedEmployeesPerDate)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void ListViewDragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void PlannedPeopleDragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void ListViewItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private async void PlannedPeopleDragDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                return;
            }

            var plannedEmployee = new PlannedEmployeesPerDate
            {
                FK_PLANNEDDAYSPERJOBORDERDETAIL = _selectedPdpjo.Id,
            };

            var listViewItem = (ListViewItem)e.Data.GetData(typeof(ListViewItem));

            if (listViewItem.Tag is Employee employee)
            {
                plannedEmployee.Employee = employee;
                plannedEmployee.FK_EMPLOYEE = employee.Id;
            }
            else if (listViewItem.Tag is Contact contact)
            {
                plannedEmployee.Contact = contact;
                plannedEmployee.FK_CONTACT = contact.Id;
            }
            else
            {
                return;
            }

            await Task.Run(
                () =>
                    {
                        _sdk.Insert(plannedEmployee);
                    });

            await RefreshData();
        }

        private async void OnShown(object sender, EventArgs e)
        {
            rootPanel.Enabled = false;
            UseWaitCursor = true;

            await InitialLoad();

            SelectInitialListViewItem(_initialSelected);

            await RefreshData();

            rootPanel.Enabled = true;
            UseWaitCursor = false;
        }

        private void SelectInitialListViewItem(int id)
        {
            foreach (ListViewItem item in lvJobOrderDetailWorkActivities.Items)
            {
                if (((PlannedDaysPerJobOrder)item.Tag).Id == id)
                {
                    item.Selected = true;
                }
            }
        }

        private async Task InitialLoad()
        {
            await Task.Run(() => {
                    _plannedWorkActivities = _sdk.Find<PlannedDaysPerJobOrder>(x => x.DATE >= _planningDate && x.DATE < _planningDate.Date.AddDays(1)).ToList();
                });

            InitializeWorkActivities();
        }

        private async Task RefreshData()
        {
            await Task.Run(() => {
                    _plannedEmployees = _sdk.Find<PlannedEmployeesPerDate>(x => x.FK_PLANNEDDAYSPERJOBORDERDETAIL == _selectedPdpjo.Id).ToList();
                    var plannedEmployees = _plannedEmployees.Where(x => x.FK_EMPLOYEE > 0).Select(x => x.FK_EMPLOYEE).ToList();
                    var plannedContacts = _plannedEmployees.Where(x => x.FK_CONTACT > 0).Select(x => x.FK_CONTACT).ToList();
                    var employeeSkills = _sdk.Find<PersonSkills>(x => x.FK_WORKACTIVITY == _selectedPdpjo.JobOrderDetailWorkActivity.WorkActivity.Id).ToList();
                    _allowedEmployees = employeeSkills.Where(x => x.FK_EMPLOYEE > 0 && !plannedEmployees.Contains(x.FK_EMPLOYEE) && !x.Employee.NIETDECLARABEL && x.Employee.EMPLOYMENTSTARTDATE <= _planningDate && (x.Employee.EMPLOYMENTTERMINATIONDATE == null || x.Employee.EMPLOYMENTTERMINATIONDATE >= _planningDate)).Select(x => x.Employee).ToList();
                    _allowedContacts = employeeSkills.Where(x => x.FK_CONTACT > 0 && !plannedContacts.Contains(x.FK_CONTACT)).Select(x => x.Contact).ToList();
                });

            InitializePlannedEmployees();
            InitializeUnplannedEmployees();
        }

        private void InitializeUnplannedEmployees()
        {
            lvEmployees.Items.Clear();
            lvZzp.Items.Clear();

            foreach (var allowedEmployee in _allowedEmployees)
            {
                lvEmployees.Items.Add(allowedEmployee.ToListViewItem());
            }

            foreach (var allowedContact in _allowedContacts)
            {
                lvZzp.Items.Add(allowedContact.ToListViewItem());
            }
        }

        private void InitializePlannedEmployees()
        {
            tvPlannedPeople.Nodes.Clear();

            var employeeNode = new TreeNode("Werknemers");
            employeeNode.Expand();
            var zzpNode = new TreeNode("ZZP'ers");
            zzpNode.Expand();

            var groups = _plannedEmployees.GroupBy(x => x.FK_EMPLOYEE.HasValue);
            foreach (var group in groups)
            {
                var mainNode = group.Key ? employeeNode : zzpNode;
                foreach (var plannedEmployee in group)
                {
                    var node = plannedEmployee.ToTreeNode();
                    mainNode.Nodes.Add(node);
                }
            }
            tvPlannedPeople.Nodes.Add(employeeNode);
            tvPlannedPeople.Nodes.Add(zzpNode);
        }

        private void InitializeWorkActivities()
        {
            lvJobOrderDetailWorkActivities.Items.Clear();
            foreach (var plannedWorkActivity in _plannedWorkActivities)
            {
                var listViewItem = plannedWorkActivity.ToListViewItem();
                lvJobOrderDetailWorkActivities.Items.Add(listViewItem);
            }
        }

        private async void LvJobOrderDetailWorkActivitiesSelectedIndexChanged(object sender, EventArgs e)
        {
            var lv = (ListView)sender;
            if (lv.SelectedItems.Count > 0)
            {
                var lvi = lv.SelectedItems[0];
                _selectedPdpjo = (PlannedDaysPerJobOrder)lvi.Tag;
            }
            await UpdateDetails();
        }

        private async Task UpdateDetails()
        {
            //get employees and planned employees
            tbJobOrder.Text = _selectedPdpjo.JobOrderDetailWorkActivity.JobOrder.Order.ORDERNUMBER + "." + _selectedPdpjo.JobOrderDetailWorkActivity.JobOrder.JOBORDERNUMBER;
            tbRelation.Text = _selectedPdpjo.JobOrderDetailWorkActivity.JobOrder.Order.Relation.NAME;
            tbOrderDescription.Text = _selectedPdpjo.JobOrderDetailWorkActivity.JobOrder.Order.DESCRIPTION;

            tbWaCode.Text = _selectedPdpjo?.JobOrderDetailWorkActivity.WorkActivity.CODE ?? "";
            tbWaDescription.Text = _selectedPdpjo?.JobOrderDetailWorkActivity.WorkActivity.DESCRIPTION ?? "";
            tbQuantity.Text = "" + _selectedPdpjo?.JobOrderDetailWorkActivity.TOTALQUANTITY;

            var tsTotalTime = TimeSpan.FromTicks(_selectedPdpjo?.JobOrderDetailWorkActivity.TOTALTIME ?? 0);
            tbHours.Text = $@"{(int)tsTotalTime.TotalHours} h {tsTotalTime.Minutes:D2} m";

            await RefreshData();
        }
        
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
