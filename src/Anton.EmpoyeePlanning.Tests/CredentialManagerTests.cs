﻿
using Anton.EmployeePlanning.CredentialManagement;
using Shouldly;

namespace Anton.EmpoyeePlanning.Tests
{
    public class CredentialManagerTests
    {
        [Xunit.FactAttribute]
        public void FactMethodName()
        {
            //var cred = new NetworkCredential("user", "password");

            //var success = CredentialManager.SaveCredentials("RidderEmployeePlanningAdministrator", cred);
            //success.ShouldBeTrue();

            var cm = CredentialManager.GetCredentials("RidderEmployeePlanningAdministrator");

            cm.ShouldNotBeNull();

            cm.UserName.ShouldBe("user");
            cm.Password.ShouldBe("password");
        }
    }
}
